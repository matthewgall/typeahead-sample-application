/**
 * Copyright (c) 2013 Matthew Gall
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial
 * portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO
 * EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR
 * THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 **/

$(document).ready( function() {
	$('div#resultPane').hide();
	$.getJSON("/api/index.php?action=getNames", function(json) {
		$('input#typeahead').typeahead({source: json})
	});
});

$('input#typeahead').change( function() {
	// We're going to update a nice prompt!
	var name = $('ul.typeahead li.active').data('value');
	var insult;

	// Now for a bit of magic...
	if ( name.length < 6 ) {
		insult = "Wow... your name is really short, which sucks!";
	}
	else if ( name.length > 8 ) {
		insult = "Jesus... how do you type that thing every morning. So long, it is like an essay!";
	}
	else {
		insult = "Your name is perfect!";
	}

	// Clear the results pane
	$('div#results').empty();
	$('div#results').append( "So... your name is <strong>" + name + "</strong> eh... " + insult );
	$('div#resultPane').show();

});